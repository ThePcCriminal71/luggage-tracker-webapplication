from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save


class UserProfile(models.Model):
	user = models.OneToOneField(User,on_delete=models.CASCADE)
	email=models.CharField(max_length=50,default='')
	uname=models.CharField(max_length=50,default='')
	pwd=models.CharField(max_length=20,default='')

def create_profile(sender,**kwargs):
	if kwargs['created']:
		user_profile=UserProfile.objects.create(user=kwargs['instance'])

post_save.connect(create_profile,sender=User)	

'''
t1
1234@5678
{% load staticfiles %}
                <script src="{% static 'syrus/check.js' %}"></script>
'''
