from django.shortcuts import render,redirect
from django.contrib.auth.forms import UserCreationForm

def home(request):
	return render(request,'syrus/home.html')
def login(request):
	return render(request,'syrus/login.html')
def register(request):
	if request.method=='POST':
		form=UserCreationForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/home')
		else:
			form=UserCreationForm()
			#args = {'form':form}
			return render(request,'syrus/register.html')
	else :
		return render(request,'syrus/register.html')
	
def about(request):
	return render(request,'syrus/about.html')
def help(request):
	return render(request,'syrus/help.html')
def Missions(request):
	return render(request,'syrus/Missions.html')
def Working(request):
	return render(request,'syrus/Working.html')