from django.contrib import admin
from syrus.models import UserProfile

# Register your models here.
admin.site.register(UserProfile)